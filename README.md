ModalAI VOXL - Development Environment
======================================

The purpose of this repo is to setup the VOXL to run the ACL snap-stack flight software. To perform the flashing and setup steps outlined below, the VOXL **must be connected via micro USB 2.0**.

## Initial Flashing and Setup

1. Follow the official [ModalAI docs](https://docs.modalai.com/voxl-quickstarts/) to [setup ADB](https://docs.modalai.com/setup-adb/) and then to [flash the VOXL system image](https://docs.modalai.com/flash-system-image/). At the time of this writing, we are using v3.3.0 (file name of `voxl_platform_3-3-0-0.4.6-b.tar.gz`). When prompted, wipe `/data/` (unless you have a reason not to) and install the voxl-suite.
2. (**ACL specific**) Download the robot SSH private key from [this](https://www.dropbox.com/sh/1553npmpuw3ha2w/AACk809EGnO22jmUHT_Oh3tYa?dl=0) password-protected Dropbox link (hint: sikorsky). Place the `id_rsa` file in the `setup/` directory. Note that this `id_rsa` private key is linked with the `aclswarm` user of GitLab/GitHub and allows you to use SSH to push/pull ACL code. If you do not have access to this private key, feel free to use your own or to ignore this step altogether. If you'd like all your commits to be under your name, feel free to use your own private key by either copying it into the `setup/` directory or generating a private/public SSH key pair as usual.
3. Proceed setting up the VOXL with the ACL automated setup script: `cd setup` and `./setup.bash`.

*If you receive an error while running `./setup.bash`, try running the script again - consider things like WiFi outages which could be causing issues.*

## Typical ACL Setup

The following steps are meant primarily as a guide for setting up your specific vehicle with additional code. These steps will setup the core snap-stack autopilot, enabling you to use, e.g., [`acl_joy`](https://gitlab.com/mit-acl/fsw/demos/acl_joy) on the base station to fly a vehicle.

1. Perform the *Initial Flashing and Setup* steps from above.
1. `cd` into the `extras` directory.
1. Install the `snapros` docker image, which is where all ROS code will be built / run from: `./install_snapros_docker.sh`
1. Configure the flight IMU (IMU0) to sample at 500 Hz, which `snap` expects: `./configure_imu0_snapstack.sh`
1. Install the snapio supporting software which allows snap-stack to communicate with IMU and motors: `./install_latest_snapio.sh`
1. Setup the `acl_ws` with `./setup_acl_ws.sh`. Note the instructions for how to use `snapros` for building and running, especially `snapros -s acl_ws` which will make `snapros` automatically source `acl_ws` upon entry into the docker container. Also note that this step will push the `fly` script - read below in *Extras* for more information about the purpose of the `fly` script.

*If at anytime you receive an error while running these steps, try running the script again - consider things like WiFi outages which could be causing issues.*

## Environment Setup

***Note:** This information is likely unimportant for most users. However, if you wish to develop any none-ROS software (e.g., install a package from source, add new hardware, etc.) then this becomes relevant.*

ModalAI provides three docker [build environments](https://docs.modalai.com/build-environments/):

- `voxl-emulator` which emulates the VOXL Yocto image and is used to build 32-bit applications that can also be linked to DSP libraries.
- `voxl-hexagon` for building DSP projects (e.g., `libsnap_io`).
- `voxl-cross` for cross-compiling armv7l (32-bit) and armv8 (64-bit) applications

Please see [Develop for VOXL in a Docker](https://docs.modalai.com/install-voxl-docker/) for more information.

## Ubuntu Docker for VOXL - snapros

***Note:** This information is useful if you'd like to understand the `snapros` Docker image build process or if you'd like to create your own custom `snapros` image. The script `install_snapros_docker.sh` will install the default `snapros` for you.*

Leveraging [Docker onboard the VOXL](https://docs.modalai.com/docker-on-voxl/) allows the use of modern Ubuntu on the VOXL. This is particularly advantageous since the VOXL runs a special stripped-down version of embedded linux which may be missing some of the tools you are familiar with (see [OpenEmbedded Yocto Project](https://www.yoctoproject.org/software-item/openembedded-core/)). To build the docker image yourself, run: `cd docker && make`.

Note that it is not required that you build the docker image yourself - using GitLab's [CI/CD pipelines](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev/-/pipelines), each time any file in the `docker` directory changes, GitLab will rebuild the image (see `.gitlab-ci.yml`). Once the most recent pipeline has passed, use `extras/install_snapros_docker.sh` to pull the image and load it onto the VOXL. An additional utility, `snapros` is created on the VOXL. Use it to (1) indicate which ROS workspace to use by default inside of the docker container (`snapros -s <workspace directory>`), (2) run commands in the container (`snapros roslaunch snap snap.launch` or simply `snapros` to drop into a `bash` session), (3) to kill the existing container (`snapros -k`), useful in case you need to restart the container.

Note that a few key environment variables and system paths are shared with the `snapros` docker container to allow seamless integration (e.g., same ROS network, `VEHTYPE`, `VEHNUM`, git credentials, ssh keys, etc.). A particularly important point is that if there is no currently running `snapros` container (i.e., `docker ps` shows nothing), then the first time you call `snapros` a container will be run as a daemon. Subsequent calls to `snapros` will use this container. This gives the impression that things like bash history and files are persistent across docker containers, but this is not true - it is actually the same container. Only data inside of `/home/root` is persistent, as it is a mount of the VOXL `/home/root` directory. After a call to `snapros -k`, the bash history inside of the container will be reset.

For info on buidling Docker images for other architectures, see [here](https://www.stereolabs.com/docs/docker/building-arm-container-on-x86/).

## Extras

- **create_tmux_ipk.sh** - *Note that `tmux` is automatically installed during `setup.bash`*. Use this script to build tmux from source for the VOXL. An `ipk` is generated which can be installed on the VOXL with `opkg install tmux_3.2a.ipk` after pushing it (e.g., `adb push tmux_3.2a.ipk /home/root`). Note that some basic tmux configurations were installed during `setup.bash`.

- **install_snapros_docker.sh** - Use this script to pull the latest Ubuntu/ROS docker image [from GitLab](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev/-/jobs) and then load it onto the VOXL, along with the `snapros` utility which enables easy access to the docker container to build and run ROS packages. NOTE: this steps takes time to complete (10-20 min), and it may not be complete even if the terminal displays "[Suc] - Found a fully-booted Snapdragon device".

- **patch_sfpro_cam.sh** - Use this script to add the driver for the sfpro imx214 hires 4k camera, which was removed circa 2021 from VOXL system image. See also [this post](https://forum.modalai.com/topic/282/voxl-and-qualcomm-flight-pro-camera-compatibility/9).

- **configure_imu0_snapstack.sh** - The [snap](https://gitlab.com/mit-acl/fsw/snap-stack/snap) autopilot uses VOXL's IMU0 for flight control (leaving [the better IMU1](https://docs.modalai.com/camera-imu-coordinate-frames/) for VIO). This script sets up IMU0 for 500 Hz sampling as expected by `snap`.

- **install_latest_snapio.sh** - The [snap](https://gitlab.com/mit-acl/fsw/snap-stack/snap) autopilot interacts with the motors via the [`ioboard`](https://gitlab.com/mit-acl/fsw/snap-stack/ioboard). This script installs the apps/dsp lib ([`libsnapio`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/libsnap_io)) which supports this. Additionally, it installs the [`snap_ipc`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/snap_ipc) package, which leverages the [Modal Pipe Architecture](https://gitlab.com/voxl-public/modal-pipe-architecture) IPC mechanism to shuttle data around with high bandwidth and low latency. This IPC mechanism allows the snapros docker image access to hardware. **Note:** this script installs these packages from the GitLab CI/CD Pipelines of [libsnap_io](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/libsnap_io/-/jobs/) and [`snap_ipc`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/snap_ipc/-/jobs) (using the `build_dev` tag - view the [script source](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev/-/blob/f481e7aafdbac5ed2a91787f38fcb0c406702398/extras/install_latest_stable_snapio.sh#L75) for more info)

- **setup_acl_ws.sh** - Clones core `snap-stack` repos into `acl_ws/src` and gives some instructions for how to use with `snapros`. More importantly, pushes the `fly` script, allowing you to easily ssh into the vehicle and run `fly` to run all the necessary ROS launch files for flight in a `tmux` session. This `fly` script is meant to be a template - you are encouraged to create your own `fly` script in your own project repo which will run your ROS nodes exactly the same way each time.

- **setup_advanced_networking.sh** - Creates a systemd service (i.e., `systemctl status network-router`) that handles creating a static ethernet connection between the VOXL and another computer. See the [wiki on advanced networking](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev/-/wikis/04.-advanced-networking) for more information.

## VOXL FAQ

- How to check internet connectivity?
    - Use `wpa_cli status` to see connectivity status. There should be an `ip_address` field. Use `ping google.com` to check for Internet access.

- Why can't `docker pull` work on the VOXL to pull the docker image directly from the [GitLab container registry](https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev/container_registry)?
    - The docker version on the VOXL is 1.9.0 (Nov 2015). Thus, it uses an older version of the registry API making it [incompatible](https://docs.gitlab.com/ee/administration/packages/container_registry.html#supporting-older-docker-clients) with GitLab's registry.

- How to check the version of packages installed?
    - Use `opkg list-installed`, e.g., `opkg list-installed | grep snap` or `opkg list-installed | grep modal` or `opkg list-installed | grep tmux`

- How to upgrade the VOXL suite?
    - You can update ModalAI software by using `opkg update` and `opkg upgrade` as explained [here](https://docs.modalai.com/voxl-suite/). **Important:** After upgrading, the `.bashrc` is overwritten. You must re-run the `setup.bash` (no need to flash, just run the script) to restore this file.

- How to change the name of an existing VOXL?
    - Connect the VOXL to your machine and run the `./setup.bash` (you can find it inside `/voxl-dev/setup/`)
    - Introduce Name (i.e. "NX") and Number (i.e "17") accordingly.
    - You need to ssh into the VOXL (using its **old name** and make sure you are connected to the same WiFi as the VOXL. Probably "RAVEN-50")
    - Once inside the VOXL, run the following commands:
        Note "VEHICLE_NAME" indicates the new name you want to put to the VOXL.
        - `hostnamectl set-hostname VEHICLE_NAME`
        - `echo 127.0.0.1 localhost.localdomain localhost VEHICLE_NAME > /etc/hosts`
    - Then, execute `adb reboot` on your machine.
    - Done!

    Note: You might see the RAVEN-50 router still detecting this device by its old name. You can change it manually on the router or just reboot router if possible for it you have its new name.

- How to fix the error 'Cannot connect to the Docker daemon. Is the docker daemon running on this host?' when trying to start `snapros`
    - removing the file '/data/network'. See https://github.com/moby/moby/issues/17083. 
    - Short explanation: docker 1.9 is not exiting correcty if snap is suddenly killed (e.g., when power-cycling). 
    - The error can be debugged further by typying '/usr/bin/docker daemon -g /data ' and observing that it ouputs the following error 'Error starting daemon: Error initializing network controller: could not delete the default bridge network: network bridge has active endpoints'

