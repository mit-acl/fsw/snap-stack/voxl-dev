#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source ../setup/utils

function usage() {
    echo
    # echo -e "$0 [--ssh HX01.local]"
    echo -e "$0"
    echo
    echo -e "\t This script sets up the advanced networking configuration which"
    echo -e "\t turns the VOXL into a router and sets up a connection between"
    echo -e "\t VOXL and a secondary computer via ethernet."
    echo -e "\t By default, adb will be used (USB required)."
    # echo -e "\t Alternatively, --ssh hostname can be used to copy using scp."
    echo
}

function parseargs() {
    # See: https://stackoverflow.com/a/29754866/2392520

    OPTIONS=h
    LONGOPTS=ssh:

    # -use ! and PIPESTATUS to get exit code with errexit set
    # -temporarily store output to be able to check for errors
    # -activate quoting/enhanced mode (e.g. by writing out “--options”)
    # -pass arguments only via   -- "$@"   to separate them correctly
    ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        # e.g. return value is 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # read getopt’s output this way to handle the quoting right:
    eval set -- "$PARSED"

    # should we use adb or ssh?
    usessh=false

    # now parse the options in order and nicely split until we see --
    while true; do
      case "$1" in
        -h)
          usage;
          exit;
          ;;
        --ssh)
          usessh=true
          HOST="$2"
          shift 2
          ;;
        --)
          shift
          break
          ;;
        *)
          echo "Invalid options"
          usage
          exit 3
          ;;
      esac
    done
}

parseargs "$@"

# adb only for now
findQualcommDevice

read -p $'\e[1;37mVehicle type (e.g., `HX`): \e[0m' VEHICLE_TYPE
read -p $'\e[1;37mVehicle num (e.g., `01`): \e[0m' VEHICLE_NUM

#
# Create the script which sets up the VOXL as a router
#

cat << EOF > /tmp/router.sh
# This SysV service adds the configuration necessary for making the sfpro
# a router (i.e., sharing a network connection with a secondary computer).
# Note that if eth0 does not exist at boot, this service does not run.
# This means that the USB to eth dongle must be plugged in at boot.
if ! ip a show eth0 > /dev/null; then
    exit 1
fi

# Set static ip address on eth0
ip a add 192.168.${VEHICLE_NUM}.1/24 brd + dev eth0

# enable ip forwarding
sysctl -w net.ipv4.ip_forward=1

# setup iptables
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
iptables -I FORWARD -o wlan0 -s 192.168.${VEHICLE_NUM}.0/24 -j ACCEPT
iptables -I INPUT -s 192.168.${VEHICLE_NUM}.0/24 -j ACCEPT
EOF
adb push /tmp/router.sh /etc/router.sh
adb shell chmod +x /etc/router.sh

#
# Setup the systemctl service to control it
#

adb push network-router.service /etc/systemd/system/
adb shell systemctl daemon-reload
adb shell systemctl enable network-router
adb shell systemctl start network-router

echo
echo_wht "Network router script has been installed"
echo
echo -e "\t~$ systemctl status network-router # check status"
echo -e "\t~$ systemctl stop network-router # doesn't actually undo anything"
echo -e "\t~$ systemctl disable network-router # on next reboot, voxl will not be router"
echo
echo_wht "Note that eth0 must be available (ethernet is plugged in) before service will run."
echo
