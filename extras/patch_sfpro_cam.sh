#!/bin/bash
#
# Use this file to add the sfpro IMX214GP hires camera driver back.
# This will likely be rectified in future (after 3.3.0) system image releases.
# For more info, see
#    https://forum.modalai.com/topic/282/voxl-and-qualcomm-flight-pro-camera-compatibility
#

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source ../setup/utils

# adb only for now
findQualcommDevice

# check if already configured
R=$(adb shell "cat /system/etc/camera/camera_config.xml | grep 0x34" | tr -d '\r')
if [ ! -z "$R" ]; then
  echo "/system/etc/camera/camera_config.xml already patched. Aborting."
  exit 1
fi

read -r -d '' CAMCONFIG <<- EOF
<!-- IMX214GP module -->
<CameraModuleConfig>
  <CameraId>0</CameraId>
  <SensorName>imx214</SensorName>
  <SensorSlaveAddress>0x34</SensorSlaveAddress>
  <FlashName>pmic</FlashName>
  <ChromatixName>imx214_chromatix</ChromatixName>
  <I2CFrequencyMode>FAST</I2CFrequencyMode>
  <ModesSupported>1</ModesSupported>
  <Position>BACK</Position>
  <MountAngle>360</MountAngle>
  <CSIInfo>
    <CSIDCore>0</CSIDCore>
    <LaneMask>0x1F</LaneMask>
    <LaneAssign>0x4320</LaneAssign>
    <ComboMode>0</ComboMode>
  </CSIInfo>
  <LensInfo>
    <FocalLength>4.73</FocalLength>
    <FNumber>2.2</FNumber>
    <TotalFocusDistance>1.9</TotalFocusDistance>
    <HorizontalViewAngle>64.1</HorizontalViewAngle>
    <VerticalViewAngle>51.6</VerticalViewAngle>
    <MinFocusDistance>0.1</MinFocusDistance>
  </LensInfo>
</CameraModuleConfig>
EOF

# find the line number that the closing tag is at
L=$(adb shell "cat /system/etc/camera/camera_config.xml | grep -in '</CameraConfigurationRoot>' | cut -d: -f1"| tr -d '\r')
L=$((L-1)) # we will insert at the *previous* line


adb shell "echo \"$CAMCONFIG\" | sed -i '${L}r /dev/stdin' /system/etc/camera/camera_config.xml"

adb reboot