#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source ../setup/utils

function usage() {
    echo
    # echo -e "$0 [--ssh HX01.local]"
    echo -e "$0"
    echo
    echo -e "\t This script installs the necessary snapio dependencies for snap-stack on VOXL."
    echo -e "\t The latest stable packages (hosted on Dropbox, but only if you put them there!)"
    echo -e "\t are downloaded on the target and installed. By default, adb will be used (USB required)."
    # echo -e "\t Alternatively, --ssh hostname can be used to copy using scp."
    echo
}

function parseargs() {
    # See: https://stackoverflow.com/a/29754866/2392520

    OPTIONS=h
    LONGOPTS=ssh:

    # -use ! and PIPESTATUS to get exit code with errexit set
    # -temporarily store output to be able to check for errors
    # -activate quoting/enhanced mode (e.g. by writing out “--options”)
    # -pass arguments only via   -- "$@"   to separate them correctly
    ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        # e.g. return value is 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # read getopt’s output this way to handle the quoting right:
    eval set -- "$PARSED"

    # should we use adb or ssh?
    usessh=false

    # now parse the options in order and nicely split until we see --
    while true; do
      case "$1" in
        -h)
          usage;
          exit;
          ;;
        --ssh)
          usessh=true
          HOST="$2"
          shift 2
          ;;
        --)
          shift
          break
          ;;
        *)
          echo "Invalid options"
          usage
          exit 3
          ;;
      esac
    done
}

parseargs "$@"

# adb only for now
findQualcommDevice

# create a dir to store all downloaded ipks
adb shell mkdir -p /tmp/ipks

# libsnap_io
adb shell curl -s -L https://gitlab.com/mit-acl/fsw/snap-stack/snapio/libsnap_io/-/jobs/artifacts/main/download?job=build_dev -o /tmp/archive.zip
adb shell unzip -qo /tmp/archive.zip -d /tmp/ipks
# snap_ipc
adb shell curl -s -L https://gitlab.com/mit-acl/fsw/snap-stack/snapio/snap_ipc/-/jobs/artifacts/main/download?job=build_dev -o /tmp/archive.zip
adb shell unzip -qo /tmp/archive.zip -d /tmp/ipks
# n.b., installs all ipks in the dir
adb shell opkg install --force-reinstall /tmp/ipks/*.ipk

# remove leftovers
adb shell rm -rf /tmp/ipks