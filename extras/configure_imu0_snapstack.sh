#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source ../setup/utils

function usage() {
    echo
    echo -e "$0 [--ssh HX01.local]"
    echo
    echo -e "\t This script configures VOXL IMU0 for snap-stack use."
    echo -e "\t By default, adb will be used (USB required)."
    echo -e "\t Alternatively, SSH can be used by specifying '--ssh hostname'."
    echo
}

function parseargs() {
    # See: https://stackoverflow.com/a/29754866/2392520

    OPTIONS=h
    LONGOPTS=ssh:

    # -use ! and PIPESTATUS to get exit code with errexit set
    # -temporarily store output to be able to check for errors
    # -activate quoting/enhanced mode (e.g. by writing out “--options”)
    # -pass arguments only via   -- "$@"   to separate them correctly
    ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        # e.g. return value is 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # read getopt’s output this way to handle the quoting right:
    eval set -- "$PARSED"

    # should we use adb or ssh?
    usessh=false

    # now parse the options in order and nicely split until we see --
    while true; do
      case "$1" in
        -h)
          usage;
          exit;
          ;;
        --ssh)
          usessh=true
          HOST="$2"
          shift 2
          ;;
        --)
          shift
          break
          ;;
        *)
          echo "Invalid options"
          usage
          exit 3
          ;;
      esac
    done
}

parseargs "$@"

# Configure IMU0 at 500 Hz sample rate, publishing every 1 sample
if $usessh; then
  ssh root@$HOST "sed -i /etc/modalai/voxl-imu-server.conf -re 's/(\"imu0_sample_rate_hz\":\s+)\w+/\1500/g'"
  ssh root@$HOST "sed -i /etc/modalai/voxl-imu-server.conf -re 's/(\"imu0_read_every_n_samples\":\s+)\w+/\11/g'"
  ssh root@$HOST "systemctl restart voxl-imu-server"
else
  findQualcommDevice
  adb shell "sed -i /etc/modalai/voxl-imu-server.conf -re 's/(\"imu0_sample_rate_hz\":\s+)\w+/\1500/g'"
  adb shell "sed -i /etc/modalai/voxl-imu-server.conf -re 's/(\"imu0_read_every_n_samples\":\s+)\w+/\11/g'"
  adb shell systemctl restart voxl-imu-server
fi

success "IMU0 configured for 500 Hz 1:1 sampling"
