#!/bin/bash

## This script is meant to be run in the voxl-emulator docker using
##    voxl-docker -i voxl-emulator
##
## It provides a tmux.ipk for install on the voxl with: opkg install tmux.ipk

ROOT=/tmp/voxl-tmux
LIBEVENT_VERSION=2.1.12
TMUX_VERSION=3.2a
IPK_NAME="tmux_${TMUX_VERSION}.ipk"

read -r -d '' TMUX_DIFF <<- EOF
diff --git a/tmux.c b/tmux.c
index 2992178..4383ccc 100644
--- a/tmux.c
+++ b/tmux.c
@@ -333,14 +333,15 @@ main(int argc, char **argv)
 	const struct options_table_entry	*oe;
 	u_int					 i;
 
-	if (setlocale(LC_CTYPE, "en_US.UTF-8") == NULL &&
-	    setlocale(LC_CTYPE, "C.UTF-8") == NULL) {
-		if (setlocale(LC_CTYPE, "") == NULL)
-			errx(1, "invalid LC_ALL, LC_CTYPE or LANG");
-		s = nl_langinfo(CODESET);
-		if (strcasecmp(s, "UTF-8") != 0 && strcasecmp(s, "UTF8") != 0)
-			errx(1, "need UTF-8 locale (LC_CTYPE) but have %s", s);
-	}
+	//if (setlocale(LC_CTYPE, "en_US.UTF-8") == NULL &&
+	//    setlocale(LC_CTYPE, "C.UTF-8") == NULL) {
+	//	if (setlocale(LC_CTYPE, "") == NULL)
+	//		errx(1, "invalid LC_ALL, LC_CTYPE or LANG");
+	//	s = nl_langinfo(CODESET);
+	//	if (strcasecmp(s, "UTF-8") != 0 && strcasecmp(s, "UTF8") != 0)
+	//		errx(1, "need UTF-8 locale (LC_CTYPE) but have %s", s);
+	//}
+	flags |= CLIENT_UTF8;
 
 	setlocale(LC_TIME, "");
 	tzset();
EOF

mkdir -p ${ROOT}
cd ${ROOT}

# create ipk structure
mkdir -p ipk/control
mkdir -p ipk/data
echo "2.0" > ipk/debian-binary
cat << EOF > ipk/control/control
Package: tmux
Version: ${TMUX_VERSION}
Section: base
Priority: optional
Architecture: all
Depends:
Conflicts:
Maintainer: plusk@mit.edu
Description: terminal multiplexer, ships with libevent ${LIBEVENT_VERSION}
EOF
cat << EOF > ipk/control/postinst
#!/bin/sh
set -e
echo ""
echo "Done installing tmux"
echo ""

EOF
# make sure there is a newline at the end ^^ or opkg will whine
chmod +x ipk/control/control
chmod +x ipk/control/postinst

# Download and build libevent
cd ${ROOT}
wget https://github.com/libevent/libevent/releases/download/release-${LIBEVENT_VERSION}-stable/libevent-${LIBEVENT_VERSION}-stable.tar.gz -O libevent.tar.gz
tar zxvf libevent.tar.gz
cd libevent-${LIBEVENT_VERSION}-stable
./configure --prefix=/usr
make
make DESTDIR=${ROOT}/ipk/data install
make install

# Download and build tmux
cd ${ROOT}
echo "$TMUX_DIFF" > tmux.diff
git clone -b ${TMUX_VERSION} https://github.com/tmux/tmux.git
cd tmux
git apply ../tmux.diff
./autogen.sh
./configure --prefix=/usr
make
make DESTDIR=${ROOT}/ipk/data install

# package ipk
cd ${ROOT}/ipk/control
tar --create --gzip -f ../control.tar.gz *
cd ${ROOT}/ipk/data
tar --create --gzip -f ../data.tar.gz *

cd ${ROOT}
ar -r $IPK_NAME ipk/control.tar.gz ipk/data.tar.gz ipk/debian-binary
mv ${IPK_NAME} $HOME/