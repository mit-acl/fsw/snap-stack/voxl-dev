#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source ../setup/utils

IMAGE=mitacl/snapros

function usage() {
    echo
    echo -e "$0 [--ssh HX01.local]"
    echo
    echo -e "\t This script pulls the latest ${IMAGE} image and loads"
    echo -e "\t it onto a VOXL. By default, adb will be used (USB required)."
    echo -e "\t Alternatively, --ssh hostname can be used to copy using scp."
    echo
}

function parseargs() {
    # See: https://stackoverflow.com/a/29754866/2392520

    OPTIONS=h
    LONGOPTS=ssh:

    # -use ! and PIPESTATUS to get exit code with errexit set
    # -temporarily store output to be able to check for errors
    # -activate quoting/enhanced mode (e.g. by writing out “--options”)
    # -pass arguments only via   -- "$@"   to separate them correctly
    ! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
    if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
        # e.g. return value is 1
        #  then getopt has complained about wrong arguments to stdout
        exit 2
    fi
    # read getopt’s output this way to handle the quoting right:
    eval set -- "$PARSED"

    # should we use adb or ssh?
    usessh=false

    # now parse the options in order and nicely split until we see --
    while true; do
      case "$1" in
        -h)
          usage;
          exit;
          ;;
        --ssh)
          usessh=true
          HOST="$2"
          shift 2
          ;;
        --)
          shift
          break
          ;;
        *)
          echo "Invalid options"
          usage
          exit 3
          ;;
      esac
    done
}

parseargs "$@"

mkdir -p snaprosimg
cd snaprosimg

# check if we should download new image
download=false
if [ ! -f snapros.tgz ] || [ ! -f version ]; then
  download=true
else
  # what is the most current version?
  wget https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev/-/jobs/artifacts/main/raw/version?job=snapros -qO /tmp/version
  THEIRVERSION=$(cat /tmp/version)
  # what is our current version?
  MYVERSION=$(cat version)
  # if different, re-download
  if [[ "$MYVERSION" != "$THEIRVERSION" ]]; then
    download=true
  fi
fi
if $download; then
  rm * > /dev/null 2>&1
  wget https://gitlab.com/mit-acl/fsw/snap-stack/voxl-dev/-/jobs/artifacts/main/download?job=snapros --show-progress -qO archive.zip
  unzip archive.zip
  rm archive.zip
fi

IMAGE_TAG=$(cat imagetag)

echo
echo "Pushing files and loading image - this could take 5-10 minutes."
echo

if $usessh; then
  scp snapros.tgz root@$HOST:/data/
  ssh root@$HOST "docker load -i /data/snapros.tgz"
  ssh root@$HOST "docker tag -f $IMAGE_TAG ${IMAGE}:latest"
  scp ../snapros root@$HOST:/usr/bin/snapros
  ssh root@$HOST "chmod +x /usr/bin/snapros"
else
  findQualcommDevice
  adb push snapros.tgz /data/
  adb shell docker load -i /data/snapros.tgz
  adb shell docker tag -f $IMAGE_TAG ${IMAGE}:latest
  adb push ../snapros /usr/bin/snapros
  adb shell chmod +x /usr/bin/snapros
fi