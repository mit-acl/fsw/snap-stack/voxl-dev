#!/bin/bash
set -e

# setup ros environment
source "/opt/ros/$ROS_DISTRO/setup.bash"

# the active ROS workspace can be set via symbolic link
if [ -L /home/root/.active_ws ]; then
  if [ -L /home/root/.active_ws/devel/setup.bash ]; then
    source /home/root/.active_ws/devel/setup.bash
  fi
fi

# run the command that was forwarded in this context
exec "$@"