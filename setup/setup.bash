#!/bin/bash

# Get path to the directory of this file, no matter where it is sourced from
MYPATH=$(dirname ${BASH_SOURCE[0]})
source $MYPATH/utils

checkRequiredFiles

echo
echo_yel "\tRemember to first flash system image using \`sudo ./install.sh\`."
echo_yel "\t    Use VOXL Platform 3.3.0 (24 June 2021) from ModalAI."
echo


read -p $'\e[1;37mVehicle type (e.g., `HX`): \e[0m' VEHICLE_TYPE
read -p $'\e[1;37mVehicle num (e.g., `01`): \e[0m' VEHICLE_NUM
VEHICLE="$VEHICLE_TYPE$VEHICLE_NUM"

# Change as desired
GCS_IP="192.168.0.19"

# First, verify that there is a Snapdragon plugged in; exit if not
findQualcommDevice

# Make sure that this is a Snapdragon 820 (APQ8096)
apqid=$( adb shell getprop ro.product.name |  tr -d '[:space:]' )
if [[ "$apqid" != "apq8096-drone" ]]; then
  fail "Please run while connected to a VOXL (or sfpro) board"
  exit
fi

echo

#
# Setup bashrc / home environment
#

if [[ $(adb shell cat /home/root/.bashrc) != *"export HOME"* ]]; then
    echo_info "Pushing .bashrc"

    adb push $MYPATH/bashrc /home/root/.bashrc
    adb shell "sed -i s/_ROSMASTER_/$GCS_IP/g /home/root/.bashrc"
    adb shell "sed -i s/_VEHTYPE_/$VEHICLE_TYPE/g /home/root/.bashrc"
    adb shell "sed -i s/_VEHNUM_/$VEHICLE_NUM/g /home/root/.bashrc"

    # setting /bin/bash as default shell
    adb shell chsh root -s /bin/bash

    echo
fi

#
# Git configuration
#

# don't overwrite if there is a .gitconfig already
if ! [ -z $(adb shell "[ -f /home/root/.gitconfig ] || echo 1") ]; then
  message "Git configuration"
  echo
  grep -A2 "\[user\]" $MYPATH/gitconfig_
  read -p "Push this git user info to $(echo -e $YELLOW$VEHICLE$ENDCOLOR)? [Y/n] " response
  echo
  if ! [[ "$response" =~ ^([nN][oO]|[nN])$ ]]; then
    echo_info "Pushing .gitconfig"

    adb push $MYPATH/gitconfig_ /home/root/.gitconfig > /dev/null

    echo
  fi
fi

#
# SSH key configuration
#

if [ -f "$MYPATH/id_rsa" ]; then
  # don't overwrite if there is an id_rsa already
  if ! [ -z $(adb shell "[ -f /home/root/.ssh/id_rsa ] || echo 1") ]; then
    echo_info "Pushing id_rsa"
    adb shell mkdir -p /home/root/.ssh > /dev/null
    adb shell chmod 0700 /home/root/.ssh > /dev/null
    adb push $MYPATH/id_rsa /home/root/.ssh/ > /dev/null
    adb shell chmod 0600 /home/root/.ssh/id_rsa > /dev/null
  fi
fi

#
# Miscellaneous Configuration
#

# set blank root password (on first ssh this is required again for some reason)
adb shell passwd -d root > /dev/null

# remove ModalAI `my_ros_env.sh`
adb shell rm /home/root/my_ros_env.sh > /dev/null 2>&1

# remove leftover ipks
adb shell rm -rf /home/root/voxl-suite-ipk

# enable voxl docker support
adb shell voxl-configure-docker-support.sh > /dev/null

#
# Push git commit hash to vehicle so we know what version was used to setup
#

GIT_VERSION_STRING=$(cd $MYPATH ; git describe --tags --abbrev=8 --always --dirty --long)
adb shell "echo '$(date)' > /voxl-dev-setup"
adb shell "echo '$GIT_VERSION_STRING' >> /voxl-dev-setup"

#
# Setup networking
#

# Turn off wifi power saving (which can introduce latency)
adb shell iw dev wlan0 set power_save off

# n.b., for whatever reason string from adb has carriage return at end
wlan_mode=$(adb shell voxl-wifi getmode | tr -d '\r')
wlan_status=$(adb shell wpa_cli status | grep wpa_state | cut -d"=" -f2 | tr -d '\r')

if [[ "$wlan_mode" != "station" || "$wlan_status" != "COMPLETED" ]]; then 
  message "Setting up networking for $YELLOW$VEHICLE"

  # Hostname
  adb shell "hostnamectl set-hostname $VEHICLE"
  adb shell "echo 127.0.0.1 localhost.localdomain localhost $VEHICLE > /etc/hosts"

  # SSID / passphrase to robot wifi
  read -p $'\e[1;37mWifi SSID: \e[0m' netssid
  read -p $'\e[1;37mSSID Passphrase: \e[0m' netpsk

  adb shell voxl-wifi station "$netssid" "$netpsk" > /dev/null
  adb reboot

  echo_info "Rebooting device into STATION mode..."
  for i in {0..6}; do
    sleep 5
    findQualcommDeviceCheck
    if [ $found == 1 ]; then
      break;
    fi
  done
  echo
  if [ $found == 0 ]; then
      fail "Device not found. Is it fully booted?"
      exit -1
  else
      success "Found a fully-booted Snapdragon device"
      echo
  fi

  # check that we are connected
  sleep 5
  wlan_status=$(adb shell wpa_cli status | grep wpa_state | cut -d"=" -f2 | tr -d '\r')
  if [[ "$wlan_status" != "COMPLETED" ]]; then
    echo_error "Could not connect to wifi"
  fi
fi

#
# Everything below needs Internet access
#

for i in {0..3}; do
  ret=$(adb shell 'ping -c1 google.com > /dev/null 2>&1; echo $?' | tr -d '\r')
  if [ "$ret" = "0" ]; then
    break;
  fi
  sleep 3
done
if [ "$ret" != "0" ]; then
  echo_error "Could not access Internet - please resolve and run setup again"
  exit -1
fi

#
# tmux configuration
#

if ! [ -z $(adb shell "[ -f /home/root/.tmux.conf ] || echo 1") ]; then
  message "tmux configuration"
  echo
  echo_info "Pushing .tmux.conf"
  adb push $MYPATH/tmux.conf /home/root/.tmux.conf
fi
if ! [ -z $(adb shell "[ -f /usr/bin/tmux ] || echo 1") ]; then
  adb shell 'cd /tmp && curl -k -L -OJ https://www.dropbox.com/s/jb30gsjzmffgw9k/tmux_3.2a.ipk?dl=1'
  adb shell opkg --force-reinstall install /tmp/*.ipk
  adb shell rm /tmp/*.ipk
fi

#
# avahi daemon
#

avahistr=$(adb shell avahi-daemon --version | tr -d '\r')
if [[ "$avahistr" != "avahi-daemon 0.6.31" ]]; then
  message "avahi configuration (for $YELLOW$VEHICLE$CYAN.local ssh access)"
  adb shell opkg remove --force-removal-of-dependent-packages avahi-daemon > /dev/null
  adb shell 'cd /tmp && curl -k -L -OJ https://www.dropbox.com/s/u6qhpwwjsot6umo/libavahi-common3_0.6.31-r11.1_aarch64.ipk?dl=1'
  adb shell 'cd /tmp && curl -k -L -OJ https://www.dropbox.com/s/3eszu5rkdhu13zo/libavahi-core7_0.6.31-r11.1_aarch64.ipk?dl=1'
  adb shell 'cd /tmp && curl -k -L -OJ https://www.dropbox.com/s/nr9npmcvc5u2xwx/libdaemon0_0.14-r0_aarch64.ipk?dl=1'
  adb shell 'cd /tmp && curl -k -L -OJ https://www.dropbox.com/s/trg3hl9nh8z5p3u/avahi-daemon_0.6.31-r11.1_aarch64.ipk?dl=1'
  adb shell 'cd /tmp && curl -k -L -OJ https://www.dropbox.com/s/51x2ljgdmwbadkt/libnss-mdns_0.10-r7_aarch64.ipk?dl=1'
  # notice -- the order of installation matters
  adb shell opkg --force-reinstall install /tmp/libavahi-common3_0.6.31-r11.1_aarch64.ipk
  adb shell opkg --force-reinstall install /tmp/libavahi-core7_0.6.31-r11.1_aarch64.ipk
  adb shell opkg --force-reinstall install /tmp/libdaemon0_0.14-r0_aarch64.ipk
  adb shell opkg --force-reinstall install /tmp/avahi-daemon_0.6.31-r11.1_aarch64.ipk
  adb shell opkg --force-reinstall install /tmp/libnss-mdns_0.10-r7_aarch64.ipk
  adb shell rm /tmp/*.ipk
fi

# -----------------------------------------------------------------------------
# Success!
success "Setup completed -- rebooting"
adb reboot